#!/usr/bin/env bash

set -e

if [ $# -lt 3 ]
then
    echo "$0 git_repo_friendly_name git_directory git_upstream_branch critical_ndays"
    echo "The command returns 1 if the last fetch happened more than the number of critical days ago"
    echo "or if anything ahead or untracked on active branch. The last argument is optional (default = 3)."
    exit 2
fi

name="$1"
git_dir="$2"
git_tag="origin/$3"
[[ $# -eq 4 ]] && critical_ndays=$4 || critical_ndays=3

cd $git_dir

code=0

# check last git fetch
if [ -f .git/FETCH_HEAD ]
then
    now=$(date +%s)
    lastfetch=$(date +%s -d @$(stat -c %Y .git/FETCH_HEAD))
    laps=$((now-lastfetch))
    ndays=$((laps/86400))
    nmonths=$((ndays/30))
    if [ $nmonths -gt 1 ]
    then
        tput setaf 1
        echo "$name was last updated $nmonths month(s) ago"
        code=1
        tput op
    elif [ $ndays -gt $critical_ndays ]
    then
        tput setaf 3
        echo "$name was last updated $ndays day(s) ago"
        code=1
        tput op
    fi

    # propose to fetch if not updated in a while
    # (adapted from https://stackoverflow.com/questions/226703/how-do-i-prompt-for-yes-no-cancel-input-in-a-linux-shell-script)
    if [ "$code" -gt 0 ] 
    then
        printf "Fetch $name (y/n)? "
        old_stty_cfg=$(stty -g)
        stty raw -echo ; answer=$(head -c 1) ; stty $old_stty_cfg
        echo $answer
        if [[ "$answer" =~ [Yy] ]]
        then
            git fetch origin && code=0
        fi  
    fi  
fi

# check if ahead/behind $git_tag
set -- $(git rev-list --left-right --count ${git_tag}..HEAD)
if [ $# -ne 2 ]; then
    echo "No such remote branch"
    exit 0
fi

if [ "$1" -gt 0 ]; then
    tput setaf 2
    echo "$name is $1 commit(s) behind $git_tag"
fi
if [ "$2" -gt 0 ]; then
    tput setaf 1
    echo "$name is $2 commit(s) ahead of $git_tag"
    code=1
fi

# check any untracked files
set -- $(git ls-files --other --directory --exclude-standard)
if [ $# -gt 0 ]; then
    tput setaf 1
    echo "$name contains untracked files"
    code=1
fi
tput op

exit $code
