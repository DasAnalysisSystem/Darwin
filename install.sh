#!/usr/bin/env bash

set -e

BASE_DIR=$PWD

# If the user insists on using SSH for everything, make sure their key is
# unlocked before doing anything else. Otherwise they won't be able to clone.
if [ ! -z "$(git config url.ssh://git@gitlab.cern.ch:7999/.insteadOf)" ]; then
    echo "+----------------------------------------------------------------------------------+"
    echo "|                              NOTE ABOUT SSH KEYS                                 |"
    echo "|                                                                                  |"
    echo "| Your git configuration redirects all clones to SSH. We will try to make it work, |"
    echo "| but it may fail if you do not use the default SSH key. In any case running cmake |"
    echo "| commands directly will require you ensure an SSH agent is running and your SSH   |"
    echo "| key is unlocked.                                                                 |"
    echo "+----------------------------------------------------------------------------------+"
    echo
    echo "===> Unlocking your SSH key..."

    # Start an ssh agent if needed
    if [ -z "$SSH_AUTH_SOCK" ] || [ -z "$SSH_AGENT_PID" ]; then
        eval $(ssh-agent)
    fi

    # Unlock the key. This will only work if the default key is used for GitLab,
    # but we have no easy way of getting this information.
    ssh-add
fi

set -x

[ -n "$NPROC" ] || export NPROC=$(nproc)

# Download and build supporting tools
cmake -B build $(correction config --cmake) "$@"
cmake --build build --target install -j$NPROC

source tools/setup.sh

if [ ! -d /cvmfs ]; then
    echo "Cannot install the Ntupliser without access to /cvmfs"
    exit
fi
if ! which apptainer &>/dev/null ; then
    echo "Cannot install the Ntupliser without apptainer"
    exit
fi

# install CMSSW in a container
# - 1st arg: CMSSW version
# - 2nd arg: corresponding jet toolbox branch
# cc7 CMS/install_CMSSW.sh CMSSW_10_6_30 jetToolbox_102X
el8 CMS/install_CMSSW.sh CMSSW_12_4_0 jetToolbox_120X
el9 CMS/install_CMSSW.sh CMSSW_14_0_14 jetToolbox_120X
